# Telegram Bot

## Abstract
A simple Docker Setup with an simple TelegramBot

## Usage

Just copy the `bin/python/sample.env` file to `bin/python/.env` and place your bot token in this file

To start the container:  
`docker-compose up -d --build`